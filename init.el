(defvar native-comp-deferred-compilation-deny-list nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-color-background "#3E4452")
 '(company-quickhelp-color-foreground "#ABB2BF")
 '(custom-enabled-themes '(doom-acario-dark))
 '(custom-safe-themes
   '("2dd4951e967990396142ec54d376cced3f135810b2b69920e77103e0bcedfba9" "3cd28471e80be3bd2657ca3f03fbb2884ab669662271794360866ab60b6cb6e6" "b3775ba758e7d31f3bb849e7c9e48ff60929a792961a2d536edec8f68c671ca5" "3d5ef3d7ed58c9ad321f05360ad8a6b24585b9c49abcee67bdcbb0fe583a6950" "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2" "97db542a8a1731ef44b60bc97406c1eb7ed4528b0d7296997cbb53969df852d6" "cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e" "a0be7a38e2de974d1598cf247f607d5c1841dbcef1ccd97cded8bea95a7c7639" "4b0e826f58b39e2ce2829fab8ca999bcdc076dec35187bf4e9a4b938cb5771dc" "fe2539ccf78f28c519541e37dc77115c6c7c2efcec18b970b16e4a4d2cd9891d" "1d44ec8ec6ec6e6be32f2f73edf398620bb721afeed50f75df6b12ccff0fbb15" "84b14a0a41bb2728568d40c545280dbe7d6891221e7fbe7c2b1c54a3f5959289" "1bddd01e6851f5c4336f7d16c56934513d41cc3d0233863760d1798e74809b4b" "d47f868fd34613bd1fc11721fe055f26fd163426a299d45ce69bef1f109e1e71" "1f1b545575c81b967879a5dddc878783e6ebcca764e4916a270f9474215289e5" "d6844d1e698d76ef048a53cefe713dbbe3af43a1362de81cdd3aefa3711eae0d" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "e6f3a4a582ffb5de0471c9b640a5f0212ccf258a987ba421ae2659f1eaa39b09" "c2aeb1bd4aa80f1e4f95746bda040aafb78b1808de07d340007ba898efa484f5" "353ffc8e6b53a91ac87b7e86bebc6796877a0b76ddfc15793e4d7880976132ae" "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b" "850bb46cc41d8a28669f78b98db04a46053eca663db71a001b40288a9b36796c" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519" "6c531d6c3dbc344045af7829a3a20a09929e6c41d7a7278963f7d3215139f6a7" "a82ab9f1308b4e10684815b08c9cac6b07d5ccb12491f44a942d845b406b0296" "028c226411a386abc7f7a0fba1a2ebfae5fe69e2a816f54898df41a6a3412bb5" "613aedadd3b9e2554f39afe760708fc3285bf594f6447822dd29f947f0775d6c" "f91395598d4cb3e2ae6a2db8527ceb83fed79dbaf007f435de3e91e5bda485fb" "1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" "3d47380bf5aa650e7b8e049e7ae54cdada54d0637e7bac39e4cc6afb44e8463b" "23c806e34594a583ea5bbf5adf9a964afe4f28b4467d28777bcba0d35aa0872e" "82ef0ab46e2e421c4bcbc891b9d80d98d090d9a43ae76eb6f199da6a0ce6a348" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "4b6b6b0a44a40f3586f0f641c25340718c7c626cbf163a78b5a399fbe0226659" "333958c446e920f5c350c4b4016908c130c3b46d590af91e1e7e2a0611f1e8c5" "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6" "0d01e1e300fcafa34ba35d5cf0a21b3b23bc4053d388e352ae6a901994597ab1" "9b54ba84f245a59af31f90bc78ed1240fca2f5a93f667ed54bbf6c6d71f664ac" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "da186cce19b5aed3f6a2316845583dbee76aea9255ea0da857d1c058ff003546" "79586dc4eb374231af28bbc36ba0880ed8e270249b07f814b0e6555bdcb71fab" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "d9646b131c4aa37f01f909fbdd5a9099389518eb68f25277ed19ba99adeb7279" "0ab2aa38f12640ecde12e01c4221d24f034807929c1f859cbca444f7b0a98b3a" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" "a41b81af6336bd822137d4341f7e16495a49b06c180d6a6417bf9fd1001b6d2b" "f9aede508e587fe21bcfc0a85e1ec7d27312d9587e686a6f5afdbb0d220eab50" "b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" default))
 '(ein:output-area-inlined-images t)
 '(helm-minibuffer-history-key "M-p")
 '(jedi:install-python-jedi-dev-command
   '("pip3" "install" "--upgrade" "git+https://github.com/davidhalter/jedi.git@master#egg=jedi"))
 '(jedi:server-command
   '("python3" "c:/Users/danig/.emacs.d/elpa/jedi-core-20191011.1750/jediepcserver.py"))
 '(package-selected-packages
   '(ibuffer-sidebar lua-mode ligature deadgrep indium projectile-ripgrep ripgrep treemacs-all-the-icons elpy diff-hl ein org-roam company-nand2tetris nand2tetris-assembler nand2tetris vhdl-tools dap-mode scss-mode company-shell ess dashboard rust-playground which-key evil-collection sublime-themes evil-goggles beacon all-the-icons-ivy-rich ivy-rich ivy-posframe lsp-pyright evil-mc centaur-tabs projectile doom-themes fish-mode emojify lsp-python-ms julia-mode rjsx-mode typescript-mode envrc nix-mode doom-modeline undo-tree rustic command-log-mode dash js2-mode web-mode impatient-mode jupyter zmq csv-mode yaml-mode ccls exec-path-from-shell lsp-mode highlight-indentation pyvenv srefactor sly-repl-ansi-color sly-asdf sly-quicklisp sly free-keys rainbow-delimiters gdscript-mode yasnippet lsp-haskell flycheck lsp-ui gitignore-mode markdown-mode haskell-mode counsel swiper ivy spaceline-all-the-icons all-the-icons-dired all-the-icons vscode-icon dired-sidebar ido-completing-read+ spaceline one-themes monokai-theme dracula-theme cider evil-smartparens magit spacemacs-theme evil paredit darcula-theme use-package))
 '(safe-local-variable-values
   '((lsp-haskell-server-path "/home/portrucci/.xmonad/start_hls.sh")
     (lsp-haskell-process-path-hie "/home/portrucci/.xmonad/start_hls.sh")
     (lsp-haskell-process-path-hie "./start_hls.sh")
     (lsp-haskell-server-path "./start_hls.sh")))
 '(warning-suppress-types '((comp) (jedi))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(font-lock-function-name-face ((t (:foreground "#50fa7b" :weight normal))))
 '(font-lock-keyword-face ((t (:foreground "#ff79c6" :weight light)))))

(setq package-selected-packages '())

;; fixes the lag experienced when using all-the-icons with dired on windows
(if (memq system-type '(windows-nt ms-dos))
    (setq inhibit-compacting-font-caches t))

(setq python-shell-interpreter "ipython")
(setq python-shell-interpreter-args "-i --simple-prompt")
;(setq server-use-tcp t)

;; makes lsp-mode faster
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024))

(setq enable-recursive-minibuffers t)
(setenv "PYTHONIOENCODING" "utf-8")
(add-to-list 'process-coding-system-alist '("python" . (utf-8 . utf-8)))
(add-to-list 'process-coding-system-alist '("elpy" . (utf-8 . utf-8)))
(add-to-list 'process-coding-system-alist '("flake8" . (utf-8 . utf-8)))
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
;; initial setup for package installation
(require 'package-init)

(require 'my-inheritenv)

(require 'keybindings)
(require 'my-custom-commands)
(require 'ui)
(require 'packages)
(require 'advices)

(add-hook 'dired-sidebar-mode-hook 'auto-revert-mode)
(add-hook 'prog-mode-hook 'show-paren-mode)
