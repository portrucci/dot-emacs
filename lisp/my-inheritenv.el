(defun my-inheritenv/surround-form (form)
  `(inheritenv ,form))

;(use-package inheritenv
;  :defer nil
;  :config
;  (advice-add #'with-current-buffer :filter-return #'my-inheritenv/surround-form)
;  (advice-add #'with-temp-buffer :filter-return #'my-inheritenv/surround-form))

(provide 'my-inheritenv)
;; my-inheritenv.el ends here
