;;; this file is responsible for configuring packages

;; to make transitioning between machines easier
(package-install-selected-packages)

;; better keybinding definition
(use-package general)

;; gets the correct path from the default shell
(use-package exec-path-from-shell
  :config
  (when (or (memq window-system '(mac ns x pgtk)) ; when starting emacs on a unix-based system
	    (daemonp)) ; or when emacs is started as a daemon
    (exec-path-from-shell-initialize)))

(use-package doom-themes
   :config
   (load-theme 'doom-acario-dark t)
   (doom-themes-org-config))

(use-package undo-tree
  :config
  (global-undo-tree-mode 1)
  :custom
  (undo-tree-auto-save-history nil))
;; vi(m) emulation
(use-package evil
  :defer nil
  :config
  (evil-mode 1)
  :custom
  (evil-want-fine-undo t)
  (evil-undo-system 'undo-tree)
  (evil-want-keybinding nil)
  :hook
  (evil-local-mode . turn-on-undo-tree-mode))

;; displays possible keybindings strating with the currently entered key-chord
(use-package which-key
  :config
  (which-key-mode))

;; makes working with pairs of parentheses, brackets, etc.
;; a lot easier, especially in lisp
(use-package smartparens
  :defer t
  :bind
  (:map smartparens-mode-map
        ("C-<right>"   . sp-forward-slurp-sexp)
        ("C-<left>"    . sp-forward-barf-sexp)
        ("C-S-<right>" . sp-backward-barf-sexp)
        ("C-S-<left>"  . sp-backward-slurp-sexp))
  :config
  (require 'smartparens-config) ; default configuration
  (require 'smartparens-python)
  (require 'smartparens-javascript)
  ;; disable pairing of single quotes in the sly repl
  (sp-local-pair '(sly-mrepl-mode) "'" "'" :actions nil)
  :hook
  (prog-mode       . smartparens-strict-mode)
  (cider-repl-mode . smartparens-strict-mode)
  (sly-mrepl-mode . smartparens-strict-mode)
  (geiser-repl-mode . smartparens-strict-mode))
;; smartparens evil-mode integration
(use-package evil-smartparens
  :config
  (evil-define-key nil evil-smartparens-mode-map
    (kbd "M-(") #'sp-wrap-round
    (kbd "M-[") #'sp-wrap-square
    (kbd "M-\"") (lambda ()

                   "Wraps the following sexp in double-quotes"
                   (interactive)
                   (sp-wrap-with-pair "\"")))
  :hook
  (smartparens-enabled . evil-smartparens-mode))
(use-package evil-mc
  :after evil
  :config
  (evil-define-key 'visual global-map
    "A" #'evil-mc-make-cursor-in-visual-selection-end
    "I" #'evil-mc-make-cursor-in-visual-selection-beg)
  (global-evil-mc-mode 1))
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; git integration
(use-package magit
  :defer nil
  :hook
  ((after-revert after-save) . magit-after-save-refresh-buffers)
  ((after-revert after-save) . magit-after-save-refresh-status))
(use-package diff-hl
  :hook
  (magit-pre-refresh . diff-hl-magit-pre-refresh)
  (magit-post-refresh . diff-hl-magit-post-refresh)
  :config
  (global-diff-hl-mode))
(use-package git-modes)

;; clojure development environment
(use-package cider
  :custom
  (nrepl-hide-special-buffers t) ; press space in switch-buffer to view them again
  :bind*
  (:map cider-repl-mode-map
        ("C-c M-o" . cider-repl-clear-buffer)
   :map cider-mode-map
        ("C-c C-c"   . nil)
        ("C-c C-c v" . cider-find-var)))

;; vscode icons
(use-package vscode-icon
  :commands
  (vscode-icon-for-file))

;; yet another sidebar
(use-package dired-sidebar
  :bind
  (("C-x C-n" . dired-sidebar-toggle-sidebar))
  :init
  (setq dired-sidebar-theme 'icons)
  (setq dired-sidebar-use-term-integration t)
  :hook
  (dired-sidebar-mode . (lambda () (display-line-numbers-mode -1))))

;; icon theme
(use-package all-the-icons
  :custom
  (all-the-icons-scale-factor 1.1))
(use-package all-the-icons-dired
  :commands
  all-the-icons-dired-mode
  :custom
  (all-the-icons-dired-monochrome nil)
  :hook
  (dired-mode . all-the-icons-dired-mode))

(use-package centaur-tabs
  :defer nil
  :bind
  (("C-<tab>"   . centaur-tabs-forward-tab)
   ("C-<iso-lefttab>" . centaur-tabs-backward-tab))
  :config
  (centaur-tabs-mode 1)
  (centaur-tabs-group-by-projectile-project)
  :custom
  (centaur-tabs-set-icons t)
  (centaur-tabs-icon-type 'all-the-icons)
  (centaur-tabs-set-bar 'under)
  (x-underline-at-descent-line t))

;; code completion
(use-package company
  :hook
  (after-init . global-company-mode)
  :config
  (define-key company-active-map (kbd "ESC") #'company-cancel)
  :custom
  (company-minimum-prefix-length 0))

(with-eval-after-load 'treesit
  (setq treesit-language-source-alist
        '((typescript "https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "typescript/src" nil nil)
          (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "tsx/src" nil nil)))
  (customize-set-variable 'treesit-font-lock-level 4))

(use-package typescript-ts-mode
  :mode
  (("\\.ts\\'" . typescript-ts-mode)
   ("\\.tsx\\'" . tsx-ts-mode))
  :hook
  ((typescript-ts-mode tsx-ts-mode) . lsp))

;(use-package company-jedi
;  :after (company)
;  :hook
;  (python-mode . (lambda ()
;		   (add-to-list 'company-backends 'company-jedi))))

;; jupyter in emacs
(use-package ein)
(use-package ob-ipython)

;; the spacemacs modeline (replaced with doom-modeline)
;;(use-package spaceline
;;  :config
;;  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state))
;;(use-package spaceline-all-the-icons
;;  :after spaceline
;;  :config
;;  (spaceline-all-the-icons-theme))

(use-package doom-modeline
  :config
  (doom-modeline-mode 1)
  :custom
  (doom-modeline-height 25)
  (doom-modeline-bar-width 1)
  (doom-modeline-icon t)
  (doom-modeline-major-mode-icon t)
  (doom-modeline-major-mode-color-icon t)
  (doom-modeline-buffer-file-name-style 'truncate-upto-project)
  (doom-modeline-buffer-state-icon t)
  (doom-modeline-buffer-modification-icon t)
  (doom-modeline-minor-modes nil)
  (doom-modeline-enable-word-count nil)
  (doom-modeline-buffer-encoding t)
  (doom-modeline-indent-info nil)
  (doom-modeline-checker-simple-format t)
  (doom-modeline-vcs-max-length 12)
  (doom-modeline-env-version t)
  (doom-modeline-irc-stylize 'identity)
  (doom-modeline-github-timer nil)
  (doom-modeline-gnus-timer nil))

;; highlight brackets according to their depth
(use-package rainbow-delimiters
  :hook
  (prog-mode . rainbow-delimiters-mode))

;; now using ivy instead
(use-package ido-completing-read+
  :defer t
  ;:init
  ;(ido-mode 1)
  ;(ido-everywhere 1)
  ;:config
  ;(ido-ubiquitous-mode 1)
  )

(use-package projectile
  :config
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map))

;; ripgrep in emacs
(use-package rg)

;; auto completion
(use-package ivy
  :config
  (ivy-mode 1)
  :custom
  (ivy-wrap t))
(use-package ivy-posframe
  :config
  (ivy-posframe-mode 1)
  :custom
  (ivy-posframe-hide-minibuffer nil)
  (ivy-posframe-parameters '((background-color . "#141414")))
  (ivy-posframe-display-functions-alist
   '((swiper-isearch . ivy-display-function-fallback)
     (t      . ivy-posframe-display-at-frame-top-center))))
;; better M-x, based on ivy
(use-package counsel
  :after ivy
  :config
  (setcdr (assoc 'counsel-M-x ivy-initial-inputs-alist) "")
  :bind
  (("M-x"   . 'counsel-M-x)
   ("C-h f" . 'counsel-describe-function)
   ("C-h v" . 'counsel-describe-variable)
   ("C-h o" . 'counsel-describe-symbol)
   ("C-x C-f" . 'counsel-find-file)))
;; better isearch, based on ivy
(use-package swiper
  :after ivy
  :config
  (evil-global-set-key 'normal "/" (lambda ()
                                     (interactive)
                                     (setq isearch-forward t) ; default value is nil for some reason
                                     (swiper-isearch))))

(use-package ivy-rich
  :after ivy
  :config
  (ivy-rich-mode 1)
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (defun ivy-rich--switch-buffer-directory! (orig-fun &rest args)
    (cl-letf (((symbol-function 'directory-file-name) #'file-name-directory))
      (apply orig-fun args)))
  (advice-add 'ivy-rich--switch-buffer-directory :around #'ivy-rich--switch-buffer-directory!))
(use-package all-the-icons-ivy-rich
  :after ivy-rich
  :config (all-the-icons-ivy-rich-mode 1))

(use-package yasnippet
  :config
  (yas-global-mode 1)
  :hook
  (yas-minor-mode . (lambda ()
                      (yas-activate-extra-mode 'fundamental-mode))))

;(use-package lsp-bridge
;  :straight
;  '(lsp-bridge
;   :type git
;   :host github
;   :repo "manateelazycat/lsp-bridge"
;   :files (:defaults "*.el" "*.py" "acm" "core" "langserver" "multiserver" "resources")
;   :build (:not compile))
;  :init
;  (global-lsp-bridge-mode))

;; language server protocol support
(use-package lsp-mode
  :bind-keymap
  ("s-a" . lsp-mode-map)
  :commands lsp
  :custom
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.7)
  (lsp-inlay-hint-enable t)
  ;; rust specific
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-server-command '("~/.emacs.d/binaries/emacs-lsp-booster rust-analyzer"))
  ;; typescript
  (lsp-clients-typescript-tls-path "~/.emacs.d/binaries/emacs-lsp-booster")
  (lsp-clients-typescript-server-args '("typescript-language-server" "--stdio"))
  ;; c/c++
  (lsp-clangd-binary-path "~/.emacs.d/binaries/emacs-lsp-booster")
  (lsp-clients-clangd-args '("clangd"))
  :hook
  ((c-mode haskell-mode rustic-mode typescript-mode python-mode scala-ts-mode) . lsp)
  (lsp-mode . lsp-inlay-hints-mode))

(use-package lsp-ui
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-sideline-delay 0.7)
  (lsp-ui-doc-delay 0.7)
  :hook
  lsp)

;;(use-package company-lsp
;;  :after (lsp-mode company)
;;  :config
;;  (push 'company-lsp company-backends))

(use-package lsp-haskell
  :custom
  (lsp-haskell-server-path "~/.emacs.d/binaries/emacs-lsp-booster haskell-language-server-wrapper"))

;; c language server
(use-package ccls
  :custom
  (ccls-executable "/home/portrucci/.emacs.d/binaries/emacs-lsp-booster")
  (ccls-args "ccls"))

(use-package highlight-indentation
  :hook
  python-mode)

(use-package flycheck)
(use-package flycheck-projectile
  :bind
  (:map flycheck-mode-map
        ("C-c ! l" . flycheck-list-errors)))

;; rust config
(use-package rustic
  :bind
  (:map rustic-mode-map
        ("M-j"       . lsp-ui-imenu)
        ("M-?"       . lsp-find-references)
        ("C-c C-c l" . flycheck-list-errors)
        ("C-c C-c a" . lsp-execute-code-action)
        ("C-c C-c r" . lsp-rename)
        ("C-c C-c q" . lsp-workspace-restart)
        ("C-c C-c Q" . lsp-workspace-shutdown)
        ("C-c C-c s" . lsp-rust-analyzer-status))
  :custom
  (rustic-format-on-save t)
  (rustic-lsp-setup-p nil)
  (rustic-lsp-server "~/.emacs.d/binaries/emacs-lsp-booster rust-analyzer")
  :hook
  (rustic . yas-minor-mode-on))

(use-package envrc
  :config
  (envrc-global-mode)
  (advice-add 'rustic-cargo-run-command :around #'envrc-propagate-environment))

;; superior lisp interaction mode (using `sly' instead)
;; (use-package slime
;;   :init
;;   (load (expand-file-name "~/quicklisp/slime-helper.el"))
;;   (load (expand-file-name "~/.roswell/helper.el"))
;;   :config
;;   (setq slime-contribs '(slime-fancy slime-company slime-asdf))
;;   (setq inferior-lisp-program "ros -L sbcl-bin -l ~/.sbclrc run"))

(use-package sly
  :custom
  (sly-contribs '(sly-fancy sly-quicklisp sly-asdf))
  (inferior-lisp-program "ros -L sbcl-bin -l ~/.sbclrc run"))

(use-package geiser)
(use-package geiser-guile)

(use-package web-mode
  :custom
  ;; was setq-default
  (indent-tabs-mode nil))

;;; general emacs configuration
(use-package simple
  :straight nil
  :config
  (auto-fill-mode)
  (set-language-environment "UTF-8")
  :custom
  (comment-auto-fill-only-comments t)
  (show-trailing-whitespace t))

(use-package dashboard
  :defer nil
  :init
  (setq dashboard-image-banner-max-width 150)
  :config
  (dashboard-setup-startup-hook)
  :custom
  (dashboard-startup-banner "~/.emacs.d/assets/emacs-vscode-logo.png")
  (initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))))

(use-package ess
  :init
  (require 'ess-site)
  :mode
  ("\\.R\\'" . R-mode))
(use-package julia-mode)
(use-package vterm) ; a dependency of julia-snail
(use-package julia-snail
  :hook
  julia-mode)

(use-package dap-mode
  :custom
  (dap-auto-configure-featues '(sessions locals breakpoints expressions repl controls tooltip))
  :config
  (dap-auto-configure-mode 1)
  (dap-register-debug-template "Rust::GDB Run Configuration"
                               (list :type "gdb"
                                     :request "launch"
                                     :name "GDB::Run"
                                     :gdbpath "rust-gdb"
                                     :target nil
                                     :cwd nil)))

(use-package org
  :straight (:type built-in)
  :hook
  (org-mode . org-indent-mode)
  :custom
  (org-agenda-files '("~/Documents/org-mode/agenda/work.org" "~/Documents/org-mode/agenda/personal.org"))
  (org-babel-load-languages
   '((emacs-lisp . t)
     (ipython . t)))
  ;(org-format-latex-options (plist-put org-format-latex-options :scale 0.07)))
  )

(use-package emacsql-sqlite-builtin)
(use-package emacsql-sqlite)
(use-package org-roam
  :custom
  (org-roam-directory (expand-file-name "~/Documents/org-roam/"))
  (org-roam-database-connector 'sqlite-builtin)
  :config
  (org-roam-db-autosync-enable))
(use-package org-roam-ui
  :custom
  (org-roam-ui-sync-theme t)
  (org-roam-ui-follow t)
  (org-roam-ui-update-on-save t)
  (org-roam-ui-open-on-start t))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

(use-package elpy
  :config
  (elpy-enable)
  (when (load "flycheck" t t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode)))
(use-package poetry
  :hook
  (elpy-mode . poetry-tracking-mode))

(use-package nand2tetris
  :defer t
  :init
  (setq nand2tetris-core-base-dir "~/Code/nand2tetris"))

(use-package haxe-mode
  :mode ("\\.hx\\'" . haxe-mode))

(use-package battle-haxe
  :hook (haxe-mode . battle-haxe-mode)
  :bind (("S-<f4>" . #'pop-global-mark)
         :map battle-haxe-mode-map
         ("<f4>" . #'battle-haxe-goto-definition)
         ("<f12>" . #'battle-haxe-helm-find-references))
  :custom
  (battle-haxe-yasnippet-completion-expansion t)
  (battle-haxe-immediate-completion nil))

(use-package scala-ts-mode
  :interpreter ("scala" . scala-ts-mode))

(use-package sbt-mode
  :commands sbt-start sbt-command)

(use-package lsp-metals)

(use-package spell-fu
  :hook
  (org-mode . (lambda ()
                (setq spell-fu-faces-exclude
                      '(org-block-begin-line
                        org-block-end-line
                        org-code
                        org-date
                        org-drawer
                        org-document-info-keyword
                        org-ellipsis
                        org-link
                        org-meta-line
                        org-properties
                        org-properties-value
                        org-special-keyword
                        org-src
                        org-tag
                        org-verbatim))
                (spell-fu-mode))))

(use-package ligature
  :config
  (global-ligature-mode t)
  (ligature-set-ligatures 't '("www"))
  (ligature-set-ligatures '(prog-mode haxe-mode) '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://")))

(provide 'packages)
;; packages.el ends here
