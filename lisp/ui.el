;; disable annoying defaults
(tool-bar-mode -1)
(menu-bar-mode -1)
(toggle-scroll-bar -1)
;; line numbers
(global-display-line-numbers-mode t)

;; maximize emacs on start
;(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; needed because when emacs doesn't disable the scrollbar
;; with toggle-scroll-bar when it's started with `emacsclient -c'
(defun init/disable-frame-scroll-bar (frame)
  (modify-frame-parameters frame
			   '((vertical-scroll-bars . nil)
			     (horizontal-scroll-bars . nil))))

(add-hook 'after-make-frame-functions 'init/disable-frame-scroll-bar)

;; set default font
(add-to-list 'default-frame-alist
             '(font . "JetBrainsMono Nerd Font-10"))

;; add opacity
(add-to-list 'default-frame-alist
             '(alpha . (100 . 80)))

(provide 'ui)
;;; ui.el ends here
