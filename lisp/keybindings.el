;; by default, when pressing ESC while company is active
;; evil also changes to normal mode, which I find
;; undesireable
(defun close-company-popup ()
  (interactive)
  (company-cancel))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "<escape>") #'close-company-popup))

(provide 'keybindings)
