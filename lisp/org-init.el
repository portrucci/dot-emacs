(set-fontset-font "fontset-default" nil
                  (font-spec :size 20 :name "Symbola"))

(set-face-attribute 'default nil
                    :stripple nil :height 130 :width 'normal :inverse-video nil
                    :box nil :strike-through nil :overline nil :underline nil
                    :slant 'normal :weight 'normal :foundry "outline" :family "Fira Code")
