;;; this is a file containing misc custom commands

(defun open-emacs-init ()
  (interactive)
  (find-file (expand-file-name "init.el" user-emacs-directory))
  (dired-sidebar-show-sidebar))

(defun toggle-line-comment ()
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

;; key binding for insert-assign that doesn't add extra spaces:
;;;###autoload
(defun r-insert-assign-space-aware ()
  (interactive)
  (just-one-space 1)
  (insert "<-")
  (just-one-space 1))
;; key binding for pipe:
;;;###autoload
(defun r-pipe-operator ()
  (interactive)
  (just-one-space 1)
  (insert "%>%")
  (just-one-space 1))
;; key binding for evaluating line or selected text:
;;;###autoload
(defun r-eval-line-or-selected ()
  (interactive)
  (if (and transient-mark-mode mark-active)
      (call-interactively 'ess-eval-region)
    (call-interactively 'ess-eval-line)))

(provide 'my-custom-commands)
;;; my-custom-commands.el ends here
