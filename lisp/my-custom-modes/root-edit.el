;;; root-edit.el --- Edit file as root       -*- lexical-binding: t -*-

(require 'seq)

(define-minor-mode root-edit-mode
  "Edit an open file with superuser privileges"
  :lighter "root"
  :group 'root-edit
  (if root-edit-mode
      (root-edit--disable)
    (root-edit--enable)))

(defconst root-edit--tramp-prefix "/sudo::")

(defun root-edit--file-path-without-tramp-prefix ()
    (let ((final-tramp-property-index
           (seq-reduce (lambda (r x)
                         (if (get-text-property x 'tramp-default buffer-file-name)
                             x r))
                       (number-sequence 0 28)
                       0)))
      (substring buffer-file-name (+ final-tramp-property-index 2))))

(defun root-edit--enable ()
  (if (not buffer-file-name)
      (message "This buffer has no associated file")
    (progn
      (find-alternate-file (concat root-edit--tramp-prefix buffer-file-name))
                                        ;(root-edit--show-warning-header)
      )))

(defun root-edit--disable ()
    (when buffer-file-name
      (find-alternate-file (root-edit--file-path-without-tramp-prefix))))

(provide 'root-edit)
;;; root-edit.el ends here
