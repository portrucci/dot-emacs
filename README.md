# My Emacs Config
I'm a novice programmer and Emacs user, and I'm using this config to learn to use both Emacs in general and Elisp in particular.

## Languages
Emacs already supports many languages OOTB, but these are the languages I configured especially
 - Clojure
 - Haskell ([hindent](https://github.com/chrisdone/hindent) is required)
 - Rust
 
 ## Information about packages
- [Evil-Mode](https://github.com/emacs-evil/evil) is enabled by default
- [Smartparens](https://github.com/Fuco1/smartparens) and [rainbow-delimiters](https://github.com/Fanael/rainbow-delimiters) are used to make life with parens, brackets etc. more manageable
- [ivy-mode](https://github.com/abo-abo/swiper) for completion along with swiper and counsel
- [dired-sidebar](https://github.com/jojojames/dired-sidebar) for dir navigation
- [spaceline](https://github.com/TheBB/spaceline) is my modeline of choice
